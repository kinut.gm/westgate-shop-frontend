export default function Flex({ children, scrollX, scrollY, style }) {
  return (
    <div
      style={{
        ...style,
        display: "flex",
        overflowX: scrollX ? "auto" : "hidden",
        flexWrap: scrollY ? "wrap" : "nowrap",
      }}
    >
      {children}
    </div>
  );
}
