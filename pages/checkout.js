import { useState } from "react";
import { Page, Container, Button } from "../components";
import {
  Steps,
  Card,
  Radio,
  Space,
  Tag,
  Button as ButtonAntd,
  Typography,
  message,
  Tooltip,
} from "antd";
import { useAuth } from "../context";
import { useRouter } from "next/router";

import { db } from "../firebase/clientApp";

import { doc } from "firebase/firestore";
import { useDocumentOnce } from "react-firebase-hooks/firestore";
import { InfoCircleFilled, WarningOutlined } from "@ant-design/icons";
import DB_Services from "../firebase/services";

import { useFlutterwave, closePaymentModal } from "flutterwave-react-v3";

const { Step } = Steps;
const { Text } = Typography;

export default function Checkout() {
  const { authUser, loading } = useAuth();
  const router = useRouter();

  const [loadingOrdering, setLoadingOrdering] = useState(false);

  const [user, userLoading, userError] = useDocumentOnce(
    doc(db, "users", authUser ? authUser.uid : "unfindalble"),
    {}
  );

  const [paymentMethod, setPaymentMethod] = useState("mpesa-now");
  const [current, setCurrent] = useState(0);

  const onChangePaymentMethod = (e) => {
    setPaymentMethod(e.target.value);
    setCurrent(2);
  };

  let customer = {};
  let amount = 0;

  try {
    const getTotal = () => {
      let tot = 0;
      user.data().cart.forEach((cartItem) => {
        if (cartItem.paid == false && cartItem.deliver_first == false) {
          tot += cartItem.quantity * cartItem.product.price;
        }
      });
      return tot;
    };

    amount = getTotal();
    customer = {
      name: user.data().displayName,
      email: user.data().email,
      phone: user.data().phoneNumber,
    };
  } catch (error) {
    console.error("An error occured");
  }

  const config = {
    public_key: "FLWPUBK-c969ce7fa59b3bd9785f5c32269890bc-X",
    tx_ref: Date.now(),
    amount,
    currency: "KES",
    payment_options: "mpesa",
    customer: {
      email: customer.email,
      phonenumber: customer.phone,
      name: customer.name,
    },
    customizations: {
      title: "Westgate Shop",
      description: "Payment for cart items",
      logo: "https://st2.depositphotos.com/4403291/7418/v/450/depositphotos_74189661-stock-illustration-online-shop-log.jpg",
    },
    // redirect_url: "/",
  };

  const handleFlutterPayment = useFlutterwave(config);

  const completeOrder = () => {
    if (paymentMethod == "mpesa-now") {
      //initiate stk , change record to paid , add transaction code

      if (user.data().email == null) {
        message.warn("Missing email.");
      } else if (user.data().phoneNumber == null) {
        message.warn("Missing phone number");
      } else if (user.data().location == null) {
        message.warn("Missing delivery point");
      } else {
        handleFlutterPayment({
          callback: (response) => {
            DB_Services.setPaidTrue({
              user: authUser.uid,
              ids: getIds(user.data()),
            }).then(() => {
              message.success("Order complete. Await packing and delivery");
              router.push("/");
            });

            closePaymentModal(); // this will close the modal programmatically
          },
          onClose: (res) => {
            return;
          },
        });
      }
    } else {
      //deliver first
      if (user.data().location == ("" || null)) {
        message.warn(`Missing delivery point`);
        router.push("/account");
      } else if (user.data().phoneNumber == ("" || null)) {
        message.warn(
          `Add your phone number to complete your order. Heading over to account page`
        );
        router.push("/account");
      } else if (
        user.data().phoneNumber != ("" || null) &&
        user.data().location != ("" || null)
      ) {
        setLoadingOrdering(true);
        DB_Services.setDeliverFirst({
          user: authUser.uid,
          ids: getIds(user.data()),
        }).then(() => {
          setLoadingOrdering(false);
          message.success("Order complete. Await packing and delivery");
          router.push("/");
        });
      }
    }
  };

  const getIds = (data) => {
    let ids = [];

    data.cart
      .filter((cartItem) => cartItem.paid == false)
      .forEach((cartItem) => {
        ids[ids.length] = cartItem.product.id;
      });

    return ids;
  };

  const Delivery = ({ data }) => {
    return (
      <div style={{ width: "100%" }}>
        <br />
        <Card
          size="small"
          title="Address details"
          style={{ width: "100%", height: 230 }}
        >
          <div style={{ color: "#707070", padding: 8 }}>
            <h3>{data.displayName}</h3>
            <p>{data.phoneNumber}</p>
            <p style={{ color: "#3F9B42" }}>{data.location}</p>
          </div>
          <div
            style={{
              padding: 8,
              display: "flex",
              width: "100%",
              position: "relative",
              justifyContent: "space-between",
            }}
          >
            {data.location == null && (
              <div>
                <Text
                  type="warning"
                  style={{
                    display: "block",
                    margin: "16px 0px",
                    width: "100%",
                  }}
                >
                  <InfoCircleFilled /> Missing delivery point{" "}
                </Text>
                <ButtonAntd
                  type="ghost"
                  block
                  style={{ width: "100%", marginBottom: 16 }}
                  onClick={() => router.push("/account")}
                >
                  Edit in account
                </ButtonAntd>
              </div>
            )}
            {data.phoneNumber == null && (
              <>
                <Text
                  type="warning"
                  style={{ display: "block", margin: "16px 0px" }}
                >
                  <InfoCircleFilled /> Missing phone number{" "}
                  <ButtonAntd
                    type="ghost"
                    style={{ float: "right" }}
                    onClick={() => router.push("/account")}
                  >
                    Edit in account
                  </ButtonAntd>
                </Text>
              </>
            )}
          </div>
        </Card>
      </div>
    );
  };

  const Payment = ({ data }) => {
    const router = useRouter();
    return (
      <div style={{ padding: "16px 0px" }}>
        {paymentMethod == "mpesa-now" && data.email == null && (
          <>
            <Text
              type="warning"
              style={{ display: "block", margin: "16px 0px" }}
            >
              <InfoCircleFilled /> Missing email{" "}
              <ButtonAntd
                type="ghost"
                style={{ float: "right" }}
                onClick={() => router.push("/account")}
              >
                Edit in account
              </ButtonAntd>
            </Text>
          </>
        )}
        <Radio.Group onChange={onChangePaymentMethod} value={paymentMethod}>
          <Space direction="vertical">
            <Radio value={"mpesa-now"}>Pay now - M-pesa</Radio>
            <Radio value={"on-delivery"}>Pay on delivery</Radio>
          </Space>
        </Radio.Group>
      </div>
    );
  };

  const Summary = ({ data }) => {
    const getTotal = () => {
      let tot = 0;
      data.cart.forEach((cartItem) => {
        if (cartItem.paid == false && cartItem.deliver_first == false) {
          tot += cartItem.quantity * cartItem.product.price;
        }
      });
      return tot;
    };

    return (
      <div>
        <br />
        <Card
          size="small"
          title="Total"
          extra={<span style={{ color: "#3F9B42" }}>Ksh. {getTotal()}</span>}
          style={{ width: "100%", marginBottom: "2rem" }}
        >
          <div style={{ color: "#707070", padding: 8 }}>
            {data.cart
              .filter(
                (cartItem) =>
                  cartItem.paid == false && cartItem.deliver_first == false
              )
              .map((cartItem, index) => {
                return (
                  <div
                    style={{
                      borderBottom: "#f1f1f1 1px dashed",
                      display: "flex",
                      justifyContent: "space-between",
                      padding: "8px 0px",
                    }}
                    key={index}
                  >
                    <Tooltip
                      title={cartItem.product.name}
                      color="#3F9B42"
                      trigger="click"
                    >
                      <Text ellipsis style={{ width: 120 }}>
                        {cartItem.product.name}
                      </Text>
                    </Tooltip>

                    <Tag color="green" style={{ height: 24, width: 24 }}>
                      {cartItem.quantity}
                    </Tag>
                    <span>@</span>
                    <span>{cartItem.product.price}</span>
                    <span>{cartItem.quantity * cartItem.product.price}</span>
                  </div>
                );
              })}
          </div>
          <br />
          <div style={{ padding: 8, marginBottom: 6 }}>Delivery fee : 0</div>

          <span style={{ padding: 8 }}>
            Payment method :{" "}
            <Tag color="green">
              {paymentMethod == "mpesa-now" ? "MPESA NOW" : "PAY ON DELIVERY"}
            </Tag>
          </span>
        </Card>
      </div>
    );
  };

  if (userLoading) return <p>Loading....</p>;

  if (typeof user.data() !== "undefined") {
    return (
      <Page title="Checkout">
        <Container
          style={{
            position: "absolute",
            top: 56,
            width: "calc(100% - 16px)",
            paddingBottom: "8rem",
          }}
        >
          <br />
          <Steps direction="vertical" size="small" current={current}>
            <Step
              title="Delivery"
              description={<Delivery data={user.data()} />}
            />
            <Step
              title="Payment"
              description={<Payment data={user.data()} />}
            />
            <Step
              title="Summary"
              description={<Summary data={user.data()} />}
            />
          </Steps>
          <ButtonAntd
            loading={loadingOrdering}
            onClick={() => completeOrder(user.data())}
            style={{
              marginBottom: "2rem",
              marginLeft: "5%",
              width: "90%",
              fontFamily: "Metropolis-Regular",
              color: "#3F9B42",
              display: "block",
              fontSize: "1.1rem",
              padding: "0.9rem",
              backgroundColor: "#E5E055",
              fontWeight: "900",
              height: 50,
              textTransform: "uppercase",
              color: "#fff",
              border: "none",
              borderRadius: "8px",
            }}
          >
            Complete order
          </ButtonAntd>
        </Container>
      </Page>
    );
  } else {
    return <p>Error</p>;
  }
}
