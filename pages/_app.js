import "../styles/globals.css";
import "antd/dist/antd.css";
import { AuthUserProvider } from "../context";
import NextNProgress from "nextjs-progressbar";

function MyApp({ Component, pageProps }) {
  return (
    <AuthUserProvider>
      <NextNProgress color="#3F9B42" />
      <Component {...pageProps} />
    </AuthUserProvider>
  );
}

export default MyApp;
