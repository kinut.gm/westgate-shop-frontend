import { useState, useEffect } from "react";
import {
  getAuth,
  GoogleAuthProvider,
  signInWithPopup,
  onAuthStateChanged,
  signOut,
  RecaptchaVerifier,
  signInWithPhoneNumber,
  grecaptcha,
} from "firebase/auth";
import DB_Services from "./services";
import { app } from "./clientApp";

const formatAuthUser = (user) => ({
  uid: user.uid,
  email: user.email,
  displayName: user.displayName,
  phoneNumber: user.phoneNumber,
  photo: user.photoURL,
});

const auth = getAuth(app);

export default function useFirebaseAuth() {
  const [authUser, setAuthUser] = useState(null);
  const [loading, setLoading] = useState(true);

  const clear = () => {
    setAuthUser(null);
    setLoading(true);
  };

  // const appVerifier = new RecaptchaVerifier("recaptcha-container", {}, auth);

  const _signOut = () => {
    return new Promise((resolve, reject) => {
      signOut(auth)
        .then(clear)
        .then(() => resolve(true))
        .catch((err) => reject(false));
    });
  };

  const signInFirebase = () => {
    return new Promise((resolve, reject) => {
      var provider = new GoogleAuthProvider();

      signInWithPopup(auth, provider)
        .then((result) => {
          // This gives you a Google Access Token. You can use it to access the Google API.
          const credential = GoogleAuthProvider.credentialFromResult(result);
          const token = credential.accessToken;
          // The signed-in user info.
          const user = result.user;
          resolve(user);
          // ...
        })
        .catch((error) => {
          // Handle Errors here.
          const errorCode = error.code;
          const errorMessage = error.message;
          // The email of the user's account used.
          const email = error.email;
          // The AuthCredential type that was used.
          const credential = GoogleAuthProvider.credentialFromError(error);
          // ...
          reject(error);
        });
    });
  };

  const signInPhone = (phoneNumber) => {
    return new Promise((resolve, reject) => {
      DB_Services.checkPhone({ phoneNumber })
        .then((val) => {
          if (val == null) {
            //create new user
            resolve(1);
          } else if (typeof val.length != "undefined" && val.length > 1) {
            resolve(2);
          } else {
            setAuthUser(val);
            resolve(val);
          }
        })
        .catch((err) => reject(err));
    });
  };

  const authStateChanged = async (authState) => {
    if (!authState) {
      setAuthUser(null);
      setLoading(false);
      return;
    }

    setLoading(true);
    var formattedUser = formatAuthUser(authState);
    setAuthUser(formattedUser);
    setLoading(false);
  };

  // listen for Firebase state change
  useEffect(() => {
    const unsubscribe = onAuthStateChanged(auth, authStateChanged);

    return () => unsubscribe();
  }, []);

  return {
    authUser,
    loading,
    signInFirebase,
    signInPhone,
    _signOut,
    setAuthUser,
  };
}
